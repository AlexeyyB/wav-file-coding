#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

#include <wavfile.h>
#include <QFileDialog>
#include <QGraphicsScene>
#include <QMessageBox>

enum{
    XOR_TAB,
    DESPERSION_TAB,
    MIX_TAB
};

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT
    
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    
private slots:
    void on_openButton_clicked();
    void on_saveButton_clicked();
    void on_encodeButton_clicked();
    void on_decodeButton_clicked();

private:
    Ui::Widget *ui;
    WavFile *wavFile;
    QGraphicsScene *plotScene;

    void showSuccessMessage();
    void showErrorMessage();
};

#endif // WIDGET_H
