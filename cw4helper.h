#ifndef CW4HELPER_H
#define CW4HELPER_H

#include <QDebug>
#include <QGraphicsScene>

#include <stdio.h>
#include <math.h>

//Типы данных
typedef quint32 cw4_dword;
typedef quint16 cw4_word;
typedef char    cw4_byte;
typedef qint32  cw4_int;
typedef qreal   cw4_double;

//коды ошибок
typedef enum{
    NO_ERRORS,
    WRONG_ARGS,
    CANT_OPEN_FILE,
    UNKNOWN_ERROR
} cw4_error;

//Заголовок wav файла
typedef struct  WAV_HEADER {
    cw4_byte    chunkId[4];
    cw4_dword   chunkSize;
    cw4_byte    format[4];

    cw4_byte    subchunk1Id[4];
    cw4_dword   subchunk1Size;
    cw4_word    audioFormat;
    cw4_word    numChanels;
    cw4_dword   sampleRate;
    cw4_dword   byteRate;
    cw4_word    blockAlign;
    cw4_word    bitsPerSample;

    cw4_byte    subchunk2Id[4];
    cw4_dword   subchunk2Size;
} wavHeader;

//Работа с битами
#define MASK_FOR_BYTE 0xff

bool getBit(cw4_int in, cw4_int index);
cw4_int getIntFromBytes(cw4_byte *bytes, cw4_int size);
cw4_byte *getBytesFromInt(cw4_int number, cw4_int size);

#define MIN(X,Y) ((X) < (Y) ? : (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? : (X) : (Y))

//Встраивание

cw4_double nonLinearTransform(cw4_double alpha, cw4_double message);
cw4_double insertion(cw4_double container, cw4_double message);
cw4_double extraction(cw4_double container, cw4_double stego);
cw4_double normolize(cw4_int number, cw4_double maxValue);
cw4_int reNormolize(cw4_double number, cw4_double maxValue);

//Кодирование

cw4_int getGamma(cw4_int gamma, cw4_int l, cw4_int twoInKDegree);
cw4_int getXorCoding(cw4_int *gamma, cw4_int l, cw4_int twoInKDegree, cw4_int value);
cw4_int getXorDecoding(cw4_int *gamma, cw4_int l, cw4_int twoInKDegree, cw4_int value);

template<class T>
cw4_error getMix(T *arr, cw4_int m, cw4_int M)
{
    T *temp = new T[M];
    //Задаем начальные условия
    cw4_int p = 0;
    temp[0] = *(arr);

    for(cw4_int i=1; i<M; i++){
        p = (p+m)%M;
        temp[p] = *(arr+i);
    }
    memcpy(arr, temp, sizeof(T)*M);
    delete[] temp;
    return NO_ERRORS;
}

template<class T>
cw4_error getRemix(T *arr, cw4_int m, cw4_int M)
{
    T *temp = new T[M];
    //Задаем начальные условия
    cw4_int p = 0;
    temp[0] = *(arr);

    for(cw4_int i=1; i<M; i++){
        p = (p+m)%M;
        temp[i] = *(arr+p);
    }
    memcpy(arr, temp, sizeof(T)*M);
    delete[] temp;
    return NO_ERRORS;
}

cw4_int getDispersion(cw4_int value, cw4_int prevValue, cw4_int twoInKDegree);
cw4_int getRedispersion(cw4_int value, cw4_int prevValue, cw4_int twoInKDegree);

cw4_int gcd(cw4_int a, cw4_int b);

#endif // CW4HELPER_H
