#include "wavfile.h"

WavFile::WavFile():header(new wavHeader)
{
}

cw4_error WavFile::openWavFile(QString *fileName)
{
    QByteArray tempByteArray = fileName->toLatin1();

    FILE *wavFile = fopen(tempByteArray.data(),"rb");
    if(wavFile == NULL) {
        return CANT_OPEN_FILE;
    }
    fread(this->header, sizeof(wavHeader), 1, wavFile);

    cw4_int chanels = this->header->numChanels;
    cw4_int blockAlign = this->header->blockAlign;
    cw4_int sampleNum = this->header->subchunk2Size/blockAlign;
    cw4_int sampleSize = blockAlign/chanels;

    this->data = new cw4_int*[sampleNum];

    for(cw4_int i=0; i<sampleNum; i++){

        this->data[i] = new cw4_int[chanels];

        for(cw4_int j=0; j<chanels; j++){
            cw4_byte *temp = new cw4_byte[sampleSize];
            fread(temp, sizeof(cw4_byte), sampleSize, wavFile);
            this->data[i][j] = getIntFromBytes(temp, sampleSize);
            delete[] temp;
        }
    }

    fclose(wavFile);
    return NO_ERRORS;
}

cw4_error WavFile::saveWavFile(QString *fileName)
{
    QByteArray tempByteArray = fileName->toLatin1();

    FILE *wavFile = fopen(tempByteArray.data(),"wb");
    if(wavFile == NULL) {
        return CANT_OPEN_FILE;
    }
    fwrite(this->header, sizeof(wavHeader), 1, wavFile);

    cw4_int chanels = this->header->numChanels;
    cw4_int blockAlign = this->header->blockAlign;
    cw4_int sampleNum = this->header->subchunk2Size/blockAlign;
    cw4_int sampleSize = blockAlign/chanels;

    for(cw4_int i=0; i<sampleNum; i++){
        for(cw4_int j=0; j<chanels; j++){
            cw4_byte *temp = getBytesFromInt(data[i][j], sampleSize);
            fwrite(temp, sizeof(cw4_byte), sampleSize, wavFile);
            delete[] temp;
        }
    }

    fclose(wavFile);
    return NO_ERRORS;
}

QString WavFile::getDescription(){
    cw4_int chanels = this->header->numChanels;
    cw4_int blockAlign = this->header->blockAlign;
    cw4_int byteRate = this->header->byteRate;
    QString format = QString("Chanels: %1\nBlock align: %2\nByte rate: %3");
    return format.arg(chanels).arg(blockAlign).arg(byteRate);
}

cw4_error WavFile::drawWaveform(QGraphicsScene *scene, const QRect frame)
{
    scene->clear();
    cw4_double maxX = frame.width();
    cw4_double maxY = frame.height()/2;

    cw4_int chanels = this->header->numChanels;
    cw4_int blockAlign = this->header->blockAlign;
    cw4_int sampleNum = this->header->subchunk2Size/blockAlign;

    cw4_int stepX = sampleNum/maxX;
    cw4_double cofY = ((cw4_double)maxY-20)/pow(2.0L, 16);

    QPen pen(Qt::green);

    cw4_double shiftY = maxY/2;

    for(cw4_int i=1; i<maxX; i++){
        for(cw4_int j=0; j<chanels; j++){
            for(cw4_int k=(i-1)*stepX; k<i*stepX; k+=stepX/10){
                cw4_double y = this->data[k][j]*cofY;
                scene->addLine(i, 0+shiftY*(-1+2*j), i, y+shiftY*(-1+2*j), pen);
            }
        }
    }

    return NO_ERRORS;
}

cw4_error WavFile::xorCoding(cw4_int gamma, cw4_int k, cw4_int l)
{
    cw4_int chanels = this->header->numChanels;
    cw4_int blockAlign = this->header->blockAlign;
    cw4_int sampleNum = this->header->subchunk2Size/blockAlign;
    cw4_int sampleSize = blockAlign/chanels;

    cw4_int twoOnKDegree = pow(2, k);
    cw4_int tempGamma = gamma;

    for(cw4_int i=0; i<sampleNum; i++){
        for(cw4_int j=0; j<chanels; j++){
            cw4_int tempInt = getXorCoding(&tempGamma, l, twoOnKDegree, this->data[i][j]);
            cw4_byte *tempByteArray = getBytesFromInt(tempInt, sampleSize);
            this->data[i][j] = getIntFromBytes(tempByteArray, sampleSize);
        }
    }
    return NO_ERRORS;
}

cw4_error WavFile::xorDecoding(cw4_int gamma, cw4_int k, cw4_int l)
{
    cw4_int chanels = this->header->numChanels;
    cw4_int blockAlign = this->header->blockAlign;
    cw4_int sampleNum = this->header->subchunk2Size/blockAlign;
    cw4_int sampleSize = blockAlign/chanels;

    cw4_int twoOnKDegree = pow(2, k);
    cw4_int tempGamma = gamma;


    for(cw4_int i=0; i<sampleNum; i++){
        for(cw4_int j=0; j<chanels; j++){
            cw4_int tempInt = getXorDecoding(&tempGamma, l, twoOnKDegree, this->data[i][j]);
            cw4_byte *tempByteArray = getBytesFromInt(tempInt, sampleSize);
            this->data[i][j] = getIntFromBytes(tempByteArray, sampleSize);
        }
    }
    return NO_ERRORS;
}

cw4_error WavFile::dispersionCoding(cw4_int k)
{
    cw4_int chanels = this->header->numChanels;
    cw4_int blockAlign = this->header->blockAlign;
    cw4_int sampleNum = this->header->subchunk2Size/blockAlign;
    cw4_int sampleSize = blockAlign/chanels;

    cw4_int twoOnKDegree = pow(2, k);

    for(cw4_int i=1; i<sampleNum; i++){
        for(cw4_int j=0; j<chanels; j++){
            cw4_int tempInt = getDispersion(this->data[i][j], this->data[i-1][j], twoOnKDegree);
            cw4_byte *tempByteArray = getBytesFromInt(tempInt, sampleSize);
            this->data[i][j] = getIntFromBytes(tempByteArray, sampleSize);
        }
    }
    return NO_ERRORS;
}

cw4_error WavFile::dispersionDecoding(cw4_int k)
{
    cw4_int chanels = this->header->numChanels;
    cw4_int blockAlign = this->header->blockAlign;
    cw4_int sampleNum = this->header->subchunk2Size/blockAlign;
    cw4_int sampleSize = blockAlign/chanels;

    cw4_int twoOnKDegree = pow(2, k);

    for(cw4_int i=sampleNum-1; i>0; i--){
        for(cw4_int j=0; j<chanels; j++){
            cw4_int tempInt = getRedispersion(this->data[i][j], this->data[i-1][j], twoOnKDegree);
            cw4_byte *tempByteArray = getBytesFromInt(tempInt, sampleSize);
            this->data[i][j] = getIntFromBytes(tempByteArray, sampleSize);
        }
    }
    return NO_ERRORS;
}

cw4_error WavFile::mixCoding(cw4_int m, cw4_int M){
    cw4_int blockAlign = this->header->blockAlign;
    cw4_int sampleNum = this->header->subchunk2Size/blockAlign;

    for(cw4_int i=0; i<sampleNum-M; i+=M){
        getMix<cw4_int*>(this->data+i, m, M);
    }
    return NO_ERRORS;
}

cw4_error WavFile::mixDecoding(cw4_int m, cw4_int M){
    cw4_int blockAlign = this->header->blockAlign;
    cw4_int sampleNum = this->header->subchunk2Size/blockAlign;

    for(cw4_int i=0; i<sampleNum-M; i+=M){
        getRemix<cw4_int*>(this->data+i, m, M);
    }
    return NO_ERRORS;
}
