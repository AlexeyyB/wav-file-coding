#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    this->wavFile = NULL;
    this->plotScene = new QGraphicsScene();
    ui->waveformPLot->setScene(this->plotScene);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_openButton_clicked()
{
    ui->fileHeader->clear();
    QString *fileName = new QString((QFileDialog::getOpenFileName(this, tr("Open WAV"), "./", tr("WAV Files (*.wav)"))));
    delete this->wavFile;
    this->wavFile = new WavFile();
    if(this->wavFile->openWavFile(fileName)){
        this->showErrorMessage();
    }else{
        ui->fileHeader->append(this->wavFile->getDescription());
        this->wavFile->drawWaveform(this->plotScene, ui->waveformPLot->geometry());
        this->showSuccessMessage();
    }
}

void Widget::on_saveButton_clicked()
{
    QString *fileName = new QString((QFileDialog::getSaveFileName(this, tr("save WAV"), "./", tr("WAV Files (*.wav)"))));
    if(this->wavFile->saveWavFile(fileName)){
        this->showErrorMessage();
    }else{
        this->showSuccessMessage();
    }
}

void Widget::on_encodeButton_clicked()
{
    cw4_error error = NO_ERRORS;
    switch(ui->tabWidget->currentIndex()){
        case DESPERSION_TAB:{
            error = this->wavFile->dispersionCoding(ui->dispersalK->value());
        }break;
        case MIX_TAB:{
            cw4_int m = ui->mixSmallM->value();
            cw4_int M = ui->mixBigM->value();
            error = this->wavFile->mixCoding(m, M);
        }break;
        default:{
            cw4_int initGamma = ui->gammaInit->value();
            cw4_int k = ui->gammaK->value();
            cw4_int l = ui->gammaL->value();
            error = this->wavFile->xorCoding(initGamma, k, l);
        }break;
    };
    if(error){
        this->showErrorMessage();
    }else{
        this->wavFile->drawWaveform(this->plotScene, ui->waveformPLot->geometry());
        this->showSuccessMessage();
    }
}

void Widget::on_decodeButton_clicked()
{
    cw4_error error = NO_ERRORS;
    switch(ui->tabWidget->currentIndex()){
        case DESPERSION_TAB:{
            error = this->wavFile->dispersionDecoding(ui->dispersalK->value());
        }break;
        case MIX_TAB:{
            cw4_int m = ui->mixSmallM->value();
            cw4_int M = ui->mixBigM->value();
            error = this->wavFile->mixDecoding(m, M);
        }break;
        default:{
            cw4_int initGamma = ui->gammaInit->value();
            cw4_int k = ui->gammaK->value();
            cw4_int l = ui->gammaL->value();
            error = this->wavFile->xorDecoding(initGamma, k, l);
        }break;
    };
    if(error){
        this->showErrorMessage();
    }else{
        this->wavFile->drawWaveform(this->plotScene, ui->waveformPLot->geometry());
        this->showSuccessMessage();
    }
}

void Widget::showSuccessMessage()
{
    QMessageBox msgBox;
    msgBox.setText("Process has been completed successfully.");
    msgBox.exec();
}

void Widget::showErrorMessage()
{
    QMessageBox msgBox;
    msgBox.setText("Error!");
    msgBox.exec();
}
