/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created: Tue Oct 16 09:56:44 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGraphicsView>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QTabWidget>
#include <QtGui/QTextEdit>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_6;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout;
    QTextEdit *fileHeader;
    QVBoxLayout *verticalLayout_2;
    QPushButton *openButton;
    QPushButton *saveButton;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *verticalLayout_3;
    QTabWidget *tabWidget;
    QWidget *xorTab;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QSpacerItem *horizontalSpacer_3;
    QSpinBox *gammaInit;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_2;
    QSpinBox *gammaK;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer;
    QSpinBox *gammaL;
    QWidget *dispersalTab;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer_4;
    QSpinBox *dispersalK;
    QWidget *mixTab;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_5;
    QSpacerItem *horizontalSpacer_5;
    QSpinBox *mixBigM;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_6;
    QSpacerItem *horizontalSpacer_6;
    QSpinBox *mixSmallM;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *encodeButton;
    QPushButton *decodeButton;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout;
    QGraphicsView *waveformPLot;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QString::fromUtf8("Widget"));
        Widget->resize(598, 466);
        verticalLayout_4 = new QVBoxLayout(Widget);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        groupBox = new QGroupBox(Widget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy);
        horizontalLayout = new QHBoxLayout(groupBox);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        fileHeader = new QTextEdit(groupBox);
        fileHeader->setObjectName(QString::fromUtf8("fileHeader"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(fileHeader->sizePolicy().hasHeightForWidth());
        fileHeader->setSizePolicy(sizePolicy1);

        horizontalLayout->addWidget(fileHeader);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        openButton = new QPushButton(groupBox);
        openButton->setObjectName(QString::fromUtf8("openButton"));

        verticalLayout_2->addWidget(openButton);

        saveButton = new QPushButton(groupBox);
        saveButton->setObjectName(QString::fromUtf8("saveButton"));

        verticalLayout_2->addWidget(saveButton);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);


        horizontalLayout->addLayout(verticalLayout_2);


        horizontalLayout_6->addWidget(groupBox);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        tabWidget = new QTabWidget(Widget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy2);
        xorTab = new QWidget();
        xorTab->setObjectName(QString::fromUtf8("xorTab"));
        verticalLayout_5 = new QVBoxLayout(xorTab);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label = new QLabel(xorTab);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy3);

        horizontalLayout_3->addWidget(label);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        gammaInit = new QSpinBox(xorTab);
        gammaInit->setObjectName(QString::fromUtf8("gammaInit"));
        sizePolicy3.setHeightForWidth(gammaInit->sizePolicy().hasHeightForWidth());
        gammaInit->setSizePolicy(sizePolicy3);
        gammaInit->setValue(5);

        horizontalLayout_3->addWidget(gammaInit);


        verticalLayout_5->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_2 = new QLabel(xorTab);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        sizePolicy3.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy3);

        horizontalLayout_4->addWidget(label_2);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);

        gammaK = new QSpinBox(xorTab);
        gammaK->setObjectName(QString::fromUtf8("gammaK"));
        sizePolicy3.setHeightForWidth(gammaK->sizePolicy().hasHeightForWidth());
        gammaK->setSizePolicy(sizePolicy3);
        gammaK->setValue(16);

        horizontalLayout_4->addWidget(gammaK);


        verticalLayout_5->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_3 = new QLabel(xorTab);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        sizePolicy3.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy3);

        horizontalLayout_5->addWidget(label_3);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer);

        gammaL = new QSpinBox(xorTab);
        gammaL->setObjectName(QString::fromUtf8("gammaL"));
        gammaL->setMaximum(32768);
        gammaL->setValue(3);

        horizontalLayout_5->addWidget(gammaL);


        verticalLayout_5->addLayout(horizontalLayout_5);

        tabWidget->addTab(xorTab, QString());
        dispersalTab = new QWidget();
        dispersalTab->setObjectName(QString::fromUtf8("dispersalTab"));
        verticalLayout_6 = new QVBoxLayout(dispersalTab);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_4 = new QLabel(dispersalTab);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_7->addWidget(label_4);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_4);

        dispersalK = new QSpinBox(dispersalTab);
        dispersalK->setObjectName(QString::fromUtf8("dispersalK"));
        dispersalK->setValue(16);

        horizontalLayout_7->addWidget(dispersalK);


        verticalLayout_6->addLayout(horizontalLayout_7);

        tabWidget->addTab(dispersalTab, QString());
        mixTab = new QWidget();
        mixTab->setObjectName(QString::fromUtf8("mixTab"));
        verticalLayout_7 = new QVBoxLayout(mixTab);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_5 = new QLabel(mixTab);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_8->addWidget(label_5);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_5);

        mixBigM = new QSpinBox(mixTab);
        mixBigM->setObjectName(QString::fromUtf8("mixBigM"));
        mixBigM->setValue(57);

        horizontalLayout_8->addWidget(mixBigM);


        verticalLayout_7->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_6 = new QLabel(mixTab);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_9->addWidget(label_6);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_6);

        mixSmallM = new QSpinBox(mixTab);
        mixSmallM->setObjectName(QString::fromUtf8("mixSmallM"));
        mixSmallM->setValue(43);

        horizontalLayout_9->addWidget(mixSmallM);


        verticalLayout_7->addLayout(horizontalLayout_9);

        tabWidget->addTab(mixTab, QString());

        verticalLayout_3->addWidget(tabWidget);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        encodeButton = new QPushButton(Widget);
        encodeButton->setObjectName(QString::fromUtf8("encodeButton"));

        horizontalLayout_2->addWidget(encodeButton);

        decodeButton = new QPushButton(Widget);
        decodeButton->setObjectName(QString::fromUtf8("decodeButton"));

        horizontalLayout_2->addWidget(decodeButton);


        verticalLayout_3->addLayout(horizontalLayout_2);


        horizontalLayout_6->addLayout(verticalLayout_3);


        verticalLayout_4->addLayout(horizontalLayout_6);

        groupBox_2 = new QGroupBox(Widget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        verticalLayout = new QVBoxLayout(groupBox_2);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        waveformPLot = new QGraphicsView(groupBox_2);
        waveformPLot->setObjectName(QString::fromUtf8("waveformPLot"));
        waveformPLot->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        waveformPLot->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::NoBrush);
        waveformPLot->setBackgroundBrush(brush);
        QBrush brush1(QColor(0, 0, 0, 255));
        brush1.setStyle(Qt::NoBrush);
        waveformPLot->setForegroundBrush(brush1);

        verticalLayout->addWidget(waveformPLot);


        verticalLayout_4->addWidget(groupBox_2);


        retranslateUi(Widget);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QApplication::translate("Widget", "Widget", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("Widget", "Audio file", 0, QApplication::UnicodeUTF8));
        openButton->setText(QApplication::translate("Widget", "Open", 0, QApplication::UnicodeUTF8));
        saveButton->setText(QApplication::translate("Widget", "Save", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Widget", "Initial gamma:", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Widget", "k:", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("Widget", "L:", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(xorTab), QApplication::translate("Widget", "XOR", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("Widget", "k:", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(dispersalTab), QApplication::translate("Widget", "Dispersal", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("Widget", "M:", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("Widget", "m:", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(mixTab), QApplication::translate("Widget", "Mixing", 0, QApplication::UnicodeUTF8));
        encodeButton->setText(QApplication::translate("Widget", "encode", 0, QApplication::UnicodeUTF8));
        decodeButton->setText(QApplication::translate("Widget", "decode", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("Widget", "Waveform", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
