#-------------------------------------------------
#
# Project created by QtCreator 2012-10-14T15:08:25
#
#-------------------------------------------------

QT       += core gui

TARGET = lab1_audio
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    cw4helper.cpp \
    wavfile.cpp

HEADERS  += widget.h \
    cw4helper.h \
    wavfile.h

FORMS    += widget.ui
