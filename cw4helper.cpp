#include <cw4helper.h>

bool getBit(cw4_int in, cw4_int index)
{
    return in & (1<<index);
}

cw4_int getIntFromBytes(cw4_byte *bytes, cw4_int size)
{
    cw4_int result = 0;
    for (cw4_int i = 0; i < size; ++i){
        if (i<size-1){
            result |= (bytes[i]&MASK_FOR_BYTE)<<(8*i);
        }else{
            result |= bytes[i]<<(8*i);
        }
    }
    return result;
}

char *getBytesFromInt(cw4_int number, cw4_int size)
{
    cw4_byte *result = new cw4_byte[size];
    for (cw4_int i = 0; i < size; ++i){
        result[i] = (cw4_byte)(number>>(8*i));
    }
    return result;
}

cw4_double nonLinearTransform(cw4_double alpha, cw4_double message)
{
    return (pow((1.0L+message),alpha) - pow((1.0L-message),alpha))/(pow((1.0L+message),alpha) + pow((1.0L-message),alpha));
}

cw4_double insertion(cw4_double container, cw4_double message)
{
    return (container + message)/(1.0L + container*message);
}

cw4_double extraction(cw4_double container, cw4_double stego)
{
    return (stego - container)/(1.0L - stego*container);
}

cw4_double normolize(cw4_int number, cw4_double maxValue)
{
    return (cw4_double)number/maxValue;
}

cw4_int reNormolize(cw4_double number, cw4_double maxValue)
{
    return (cw4_int)(number*maxValue);
}

cw4_int getGamma(cw4_int gamma, cw4_int l, cw4_int twoInKDegree)
{
    return (gamma + l)%twoInKDegree;
}

cw4_int getXorCoding(cw4_int *gamma, cw4_int l, cw4_int twoInKDegree, cw4_int value)
{
    *gamma = getGamma(*gamma, l, twoInKDegree);
    return (value + *gamma)%twoInKDegree;
}

cw4_int getXorDecoding(cw4_int *gamma, cw4_int l, cw4_int twoInKDegree, cw4_int value)
{
    *gamma = getGamma(*gamma, l, twoInKDegree);
    return (value - *gamma)%twoInKDegree;
}

cw4_int getDispersion(cw4_int value, cw4_int prevValue, cw4_int twoInKDegree)
{
    return (value + prevValue)%twoInKDegree;
}

cw4_int getRedispersion(cw4_int value, cw4_int prevValue, cw4_int twoInKDegree)
{
    return (value - prevValue)%twoInKDegree;
}

cw4_int gcd(cw4_int a, cw4_int b)
{
  return b ? gcd(b ,a%b) : a;
}

