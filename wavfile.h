#ifndef WAVFILE_H
#define WAVFILE_H

#include <cw4helper.h>

class WavFile
{
    wavHeader *header;
    cw4_int **data;
public:
    WavFile();

    cw4_error openWavFile(QString *fileName);
    cw4_error saveWavFile(QString *fileName);
    QString getDescription();

    cw4_error drawWaveform(QGraphicsScene *scene, const QRect frame);

    cw4_error xorCoding(cw4_int gamma, cw4_int k, cw4_int l);
    cw4_error xorDecoding(cw4_int gamma, cw4_int k, cw4_int l);

    cw4_error dispersionCoding(cw4_int k);
    cw4_error dispersionDecoding(cw4_int k);

    cw4_error mixCoding(cw4_int m, cw4_int M);
    cw4_error mixDecoding(cw4_int m, cw4_int M);
};

#endif // WAVFILE_H
